﻿using System;

namespace MorseCodeTranslator
{
    /// <summary>
    /// Class provider than defines how dot, dash, symbols separator and words separator look in morse message.
    /// </summary>
    public class MorseCodeStyle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MorseCodeStyle"/> class.
        /// Standart look of morse code.
        /// </summary>
        public MorseCodeStyle()
            : this('.', '-', ' ', '|')
        {
        }

        public MorseCodeStyle(char dot, char dash, char symbolsSeparator, char wordsSeparator)
        {
            this.Dot = dot;
            this.Dash = dash;
            this.SymbolsSeparator = symbolsSeparator;
            this.WordsSeparator = wordsSeparator;
        }

        public static MorseCodeStyle StandartMorseCodeStyle => new MorseCodeStyle();

        /// <summary>
        /// Gets dot in morse code message.
        /// </summary>
        public char Dot { get; }

        /// <summary>
        /// Gets dash in morse code message.
        /// </summary>
        public char Dash { get; }

        /// <summary>
        /// Gets symbol separator in morse code message.
        /// Should be put between coded text symbols in morse code.
        /// </summary>
        public char SymbolsSeparator { get; }

        /// <summary>
        /// Gets words separator in morse code message.
        /// Should be put between coded text words in morse code.
        /// </summary>
        public char WordsSeparator { get; }
    }
}
