﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MorseCodeTranslator
{
    public static class Translator
    {
        /// <summary>
        /// Translates text into morse code using standart morse code style.
        /// Allowed symbols are letters, numbers and ' . , ? ! ( ) : ; + - = " / @ _ symbols.
        /// Morse code is built using dot, dash, symbols separator and words separator from <see cref="MorseCodeStyle"/>.
        /// This method calls <see cref="TranslateTextToMorse(string, MorseCodeStyle)"/> method.
        /// </summary>
        /// <param name="textMessage">Text message to translate.</param>
        /// <returns>Morse code.</returns>
        public static string TranslateTextToMorse(this string textMessage)

            // TODO: implement a method. Use MorseCodes class for translation.
            => textMessage.TranslateTextToMorse(MorseCodeStyle.StandartMorseCodeStyle);

        /// <summary>
        /// Translates text into morse code.
        /// Allowed symbols are letters, numbers and ' . , ? ! ( ) : ; + - = " / @ _ symbols.
        /// Morse code is built using dot, dash, symbols separator and words separator from <see cref="MorseCodeStyle"/>.
        /// </summary>
        /// <param name="textMessage">Text message to translate.</param>
        /// <param name="morseCodeStyle">Tells how dot, dash and separator look.</param>
        /// <returns>Morse code.</returns>
        public static string TranslateTextToMorse(this string textMessage, MorseCodeStyle morseCodeStyle)
        {
            // TODO: implement a method. Use MorseCodes class for translation.
            switch (textMessage)
            {
                case null:
                    throw new ArgumentNullException(nameof(textMessage));
                case "":
                    return textMessage;
            }

            if (morseCodeStyle == null)
            {
                throw new ArgumentNullException(nameof(morseCodeStyle));
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < textMessage.Length; ++i)
            {
                switch (textMessage[i])
                {
                    case ' ':
                        result.Append(morseCodeStyle.WordsSeparator);
                        break;
                    default:
                        if (i != 0 && textMessage[i - 1] != ' ')
                        {
                            result.Append(morseCodeStyle.SymbolsSeparator);
                        }

                        result.Append(textMessage[i].GetStandartMorseCodeSymbol().TurnIntoCustomMorseCode(morseCodeStyle));
                        break;
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Translates morse code into text using standart morse code style.
        /// This method calls <see cref="TranslateMorseToText(string, MorseCodeStyle)"/> method.
        /// </summary>
        /// <param name="morseMessage">Morse message to translate.</param>
        /// <returns>Text message.</returns>
        public static string TranslateMorseToText(this string morseMessage)

            // TODO: implement a method. Use MorseCodes class for translation.
            => morseMessage.TranslateMorseToText(MorseCodeStyle.StandartMorseCodeStyle);

        /// <summary>
        /// Translates morse code into text using custom morse code style.
        /// </summary>
        /// <param name="morseMessage">Morse message to translate.</param>
        /// <param name="morseCodeStyle">Tells how dot, dash, separator for symbols and words look.</param>
        /// <returns>Text message.</returns>
        public static string TranslateMorseToText(this string morseMessage, MorseCodeStyle morseCodeStyle)
        {
            // TODO: implement a method. Use MorseCodes class for translation.
            switch (morseMessage)
            {
                case null:
                    throw new ArgumentNullException(nameof(morseMessage));
                case "":
                    return morseMessage;
            }

            if (morseCodeStyle == null)
            {
                throw new ArgumentNullException(nameof(morseCodeStyle));
            }

            StringBuilder resultText = new StringBuilder();
            StringBuilder symbolInMorseCode = new StringBuilder();
            morseMessage.ValidateMorseCode(morseCodeStyle);
            for (int i = 0; i < morseMessage.Length; ++i)
            {
                if (morseMessage[i] == morseCodeStyle.Dot || morseMessage[i] == morseCodeStyle.Dash)
                {
                    symbolInMorseCode.Append(morseMessage[i]);
                }
                else
                {
                    resultText.Append(symbolInMorseCode.TurnIntoStandartMorseCode(morseCodeStyle).GetTextSymbol());
                    symbolInMorseCode.Clear();
                    if (morseMessage[i] == morseCodeStyle.WordsSeparator)
                    {
                        resultText.Append(' ');
                    }
                    else if (morseMessage[i] != morseCodeStyle.SymbolsSeparator)
                    {
                        throw new ArgumentException(nameof(morseMessage) + "contans invalid symol separator");
                    }
                }
            }

            if (symbolInMorseCode.Length != 0)
            {
                resultText.Append(symbolInMorseCode.TurnIntoStandartMorseCode(morseCodeStyle).GetTextSymbol());
            }

            return resultText.ToString();
        }

        /// <summary>
        /// Translates standart morse code into BitArray, which indicates different signals dutations and pauses between them.
        /// One bool represents the duration of dot.
        /// True means the signal is on, false - off.
        /// Duration of dash equals three durations of dot.
        /// Dots and dashes within a coded character are separated by signal of absence, called a space, equal to one dot duration.
        /// The letters of a coded word are separated by a space of duration equal to three dots, and the words are separated by a space equal to seven dots.
        /// This method calls <see cref="TranslateMorseToBitArray(string, MorseCodeStyle)"/> method.
        /// </summary>
        /// <param name="morseMessage">Message to translate.</param>
        /// <returns>Bit array.</returns>
        public static BitArray TranslateMorseToBitArray(this string morseMessage)
            => morseMessage.TranslateMorseToBitArray(MorseCodeStyle.StandartMorseCodeStyle);

        /// <summary>
        /// Translates custom morse code into bool array, which indicates different signals dutations and pauses between them.
        /// One bool represents the duration of dot.
        /// True means the signal is in, false - off.
        /// Duration of dash equals three durations of dot.
        /// Each dot or dash within a character is followed by period of signal absence, called a space, equal to the dot duration.
        /// The letters of a word are separated by a space of duration equal to three dots, and the words are separated by a space equal to seven dots.
        /// </summary>
        /// <param name="morseMessage">Message to translate.</param>
        /// <param name="morseCodeStyle">Tells how dot, dash, separator for symbols and words look.</param>
        /// <returns>Bit array.</returns>
        public static BitArray TranslateMorseToBitArray(this string morseMessage, MorseCodeStyle morseCodeStyle)
        {
            // TODO: implement a method.
            switch (morseMessage)
            {
                case null:
                    throw new ArgumentNullException(nameof(morseMessage));
                case "":
                    return new BitArray(0);
            }

            if (morseCodeStyle == null)
            {
                throw new ArgumentNullException(nameof(morseCodeStyle));
            }

            List<bool> result = new List<bool>();
            for (int i = 0; i < morseMessage.Length; ++i)
            {
                if (i != 0 &&
                   (morseMessage[i] == morseCodeStyle.Dot || morseMessage[i] == morseCodeStyle.Dash) &&
                   (morseMessage[i - 1] == morseCodeStyle.Dot || morseMessage[i - 1] == morseCodeStyle.Dash))
                {
                    result.Add(false);
                }

                result.AddRange(GetMorseCodeUnitInBits(morseMessage[i], morseCodeStyle));
            }

            return new BitArray(result.ToArray());
        }

        /// <summary>
        /// Translates bit array into standart morse code.
        /// Bit array indicates different signals dutations and pauses between them.
        /// One bool represents the duration of dot.
        /// True means the signal is in, false - off.
        /// Duration of dash equals three durations of dot.
        /// Each dot or dash within a character is followed by period of signal absence, called a space, equal to the dot duration.
        /// The letters of a word are separated by a space of duration equal to three dots, and the words are separated by a space equal to seven dots.
        /// This method calls <see cref="TranslateBitArrayToMorse(BitArray, MorseCodeStyle)"/> method.
        /// </summary>
        /// <param name="morseMessage">Message to translate.</param>
        /// <returns>Morse message.</returns>
        public static string TranslateBitArrayToMorse(this BitArray morseMessage)
            => morseMessage.TranslateBitArrayToMorse(MorseCodeStyle.StandartMorseCodeStyle);

        /// <summary>
        /// Translates bit array into custom morse code.
        /// Bit array Indicates different signals dutations and pauses between them.
        /// One bool represents the duration of dot.
        /// True means the signal is in, false - off.
        /// Duration of dash equals three durations of dot.
        /// Each dot or dash within a character is followed by period of signal absence, called a space, equal to the dot duration.
        /// The letters of a word are separated by a space of duration equal to three dots, and the words are separated by a space equal to seven dots.
        /// </summary>
        /// <param name="morseMessage">Message to translate.</param>
        /// <param name="morseCodeStyle">Tells how dot, dash, separator for symbols and words look.</param>
        /// <returns>Morse message.</returns>
        public static string TranslateBitArrayToMorse(this BitArray morseMessage, MorseCodeStyle morseCodeStyle)
        {
            if (morseMessage == null)
            {
                throw new ArgumentNullException(nameof(morseMessage));
            }

            if (morseCodeStyle == null)
            {
                throw new ArgumentNullException(nameof(morseCodeStyle));
            }

            if (morseMessage.Length == 0)
            {
                return string.Empty;
            }

            ValidateMorseCodeInBits(morseMessage);
            char? currentSymbol;
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < morseMessage.Length;)
            {
                currentSymbol = GetMorseCodeUnit(morseMessage, ref i, morseCodeStyle);
                if (currentSymbol != null)
                {
                    result.Append(currentSymbol);
                }
            }

            ValidateMorseCode(result.ToString(), morseCodeStyle);
            return result.ToString();
        }

        private static string GetStandartMorseCodeSymbol(this char textSymbol)
        {
            char upperSymbol = char.ToUpperInvariant(textSymbol);
            for (int i = 0; i < MorseCodes.CodeTable.Length; ++i)
            {
                if (upperSymbol == MorseCodes.CodeTable[i].symbol)
                {
                    return MorseCodes.CodeTable[i].morseSymbol;
                }
            }

            throw new ArgumentException(nameof(textSymbol) + " cannot be coded into morse code");
        }

        private static char GetTextSymbol(this string standartMorseCodeSymbol)
        {
            for (int i = 0; i < MorseCodes.CodeTable.Length; ++i)
            {
                if (standartMorseCodeSymbol == MorseCodes.CodeTable[i].morseSymbol)
                {
                    return MorseCodes.CodeTable[i].symbol;
                }
            }

            throw new ArgumentException(nameof(standartMorseCodeSymbol) + " is not a valid morse code symbol");
        }

        private static string TurnIntoCustomMorseCode(this string standartMorseCodeSymbol, MorseCodeStyle morseCodeStyle)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < standartMorseCodeSymbol.Length; ++i)
            {
                result.Append(standartMorseCodeSymbol[i] switch
                {
                    '.' => morseCodeStyle.Dot,
                    '-' => morseCodeStyle.Dash,
                    _ => throw new ArgumentException(nameof(standartMorseCodeSymbol) + " is not a morse code representation of symbol")
                });
            }

            return result.ToString();
        }

        private static string TurnIntoStandartMorseCode(this StringBuilder customMorseCodeSymbol, MorseCodeStyle morseCodeStyle)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < customMorseCodeSymbol.Length; ++i)
            {
                result.Append(customMorseCodeSymbol[i] switch
                {
                    _ when customMorseCodeSymbol[i] == morseCodeStyle.Dot => '.',
                    _ when customMorseCodeSymbol[i] == morseCodeStyle.Dash => '-',
                    _ => throw new ArgumentException(nameof(customMorseCodeSymbol) + " is not a morse code representation of symbol")
                });
            }

            return result.ToString();
        }

        /// <summary>
        /// Throws <see cref="ArgumentException"/> if <paramref name="morseCode"/>
        /// consist of only one symbols separator symbol
        /// or
        /// contains a symbol separator at the beginning or at the end.
        /// </summary>
        /// <param name="morseCode">Source.</param>
        /// <param name="morseCodeStyle">Morse code style.</param>
        private static void ValidateMorseCode(this string morseCode, MorseCodeStyle morseCodeStyle)
        {
            if (morseCode.Length == 1)
            {
                if (morseCode[0] == morseCodeStyle.SymbolsSeparator)
                {
                    throw new ArgumentException(nameof(morseCode) + " cannot have consist of only 1 symbols separator symbol");
                }
            }
            else
            {
                if (morseCode[0] == morseCodeStyle.SymbolsSeparator || morseCode[^1] == morseCodeStyle.SymbolsSeparator)
                {
                    throw new ArgumentException(nameof(morseCode) + " cannot have symbols separator as first or last symbol");
                }
            }
        }

        /// <summary>
        /// Throws <see cref="ArgumentException"/> if <paramref name="morseCodeInBits"/>
        /// consist of only one false or only three false
        /// or
        /// contains a single false at the beginning or at the end
        /// or
        /// contains a triple false at the beginning or at the end.
        /// </summary>
        /// <param name="morseCodeInBits">Source.</param>
        private static void ValidateMorseCodeInBits(this BitArray morseCodeInBits)
        {
            if (morseCodeInBits.Count == 1 && morseCodeInBits[0] == false)
            {
                throw new ArgumentException(nameof(morseCodeInBits) + " cannot consist of only one false");
            }
            else if (morseCodeInBits.Count == 3 && morseCodeInBits[0] == false && morseCodeInBits[1] == false && morseCodeInBits[2] == false)
            {
                throw new ArgumentException(nameof(morseCodeInBits) + " cannot consist of only three false");
            }
            else if (morseCodeInBits.Count > 3 && IsTripleFalseAtBeginningOrEnd(morseCodeInBits))
            {
                throw new ArgumentException(nameof(morseCodeInBits) + " cannot have triple false at the beginning or at the end");
            }
            else if (IsSingleFalseAtBeginningOrEnd(morseCodeInBits))
            {
                throw new ArgumentException(nameof(morseCodeInBits) + " cannot have single false as first or last element");
            }
        }

        private static bool IsSingleFalseAtBeginningOrEnd(this BitArray morseCodeInBits)
            => (morseCodeInBits[0] == false && morseCodeInBits[1] == true) ||
               (morseCodeInBits[^1] == false && morseCodeInBits[^2] == true);

        private static bool IsTripleFalseAtBeginningOrEnd(this BitArray morseCodeInBits)
            => (morseCodeInBits[0] == false && morseCodeInBits[1] == false && morseCodeInBits[2] == false && morseCodeInBits[3] == true) ||
               (morseCodeInBits[^1] == false && morseCodeInBits[^2] == false && morseCodeInBits[^3] == false && morseCodeInBits[^4] == true);

        /// <summary>
        /// Gets morse code unit in bits from char morse code unit.
        /// </summary>
        /// <param name="morseCodeUnit">Source.</param>
        /// <param name="morseCodeStyle">Morse code style.</param>
        /// <returns>Morse code unit in bits.</returns>
        private static bool[] GetMorseCodeUnitInBits(this char morseCodeUnit, MorseCodeStyle morseCodeStyle) => morseCodeUnit switch
        {
            _ when morseCodeUnit == morseCodeStyle.Dot => new bool[] { true },
            _ when morseCodeUnit == morseCodeStyle.Dash => new bool[] { true, true, true },
            _ when morseCodeUnit == morseCodeStyle.SymbolsSeparator => new bool[] { false, false, false },
            _ when morseCodeUnit == morseCodeStyle.WordsSeparator => new bool[] { false, false, false, false, false, false, false },
            _ => throw new ArgumentException(nameof(morseCodeUnit) + " is not valid")
        };

        /// <summary>
        /// Gets morse code unit starting from index <paramref name="i"/> with equal bits
        /// and translates in into char morse code unit.
        /// </summary>
        /// <param name="morseCode">Sorse.</param>
        /// <param name="i">Ref start index.</param>
        /// <param name="morseCodeStyle">Morse code style.</param>
        /// <returns>
        /// Char morse code if morse code unit is dot, dash, letter or words separator.
        /// Null if morse code unit is separator whithin one symbol or not valid.
        /// </returns>
        private static char? GetMorseCodeUnit(this BitArray morseCode, ref int i, MorseCodeStyle morseCodeStyle)
        {
            bool value = morseCode[i];
            List<bool> result = new List<bool>();
            for (; i < morseCode.Count && morseCode[i] == value; ++i)
            {
                result.Add(morseCode[i]);
            }

            IStructuralEquatable structuralEquatableResult = result.ToArray();
            return structuralEquatableResult switch
                {
                    _ when structuralEquatableResult.Equals(
                        new bool[] { false },
                        StructuralComparisons.StructuralEqualityComparer)
                    => null,
                    _ when structuralEquatableResult.Equals(
                        new bool[] { true },
                        StructuralComparisons.StructuralEqualityComparer)
                    => morseCodeStyle.Dot,
                    _ when structuralEquatableResult.Equals(
                        new bool[] { true, true, true },
                        StructuralComparisons.StructuralEqualityComparer)
                    => morseCodeStyle.Dash,
                    _ when structuralEquatableResult.Equals(
                        new bool[] { false, false, false },
                        StructuralComparisons.StructuralEqualityComparer)
                    => morseCodeStyle.SymbolsSeparator,
                    _ when structuralEquatableResult.Equals(
                        new bool[] { false, false, false, false, false, false, false },
                        StructuralComparisons.StructuralEqualityComparer)
                    => morseCodeStyle.WordsSeparator,
                    _ => throw new ArgumentException(nameof(morseCode) + " is invalid")
                };
            }
    }
}
